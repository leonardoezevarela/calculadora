let suma = () => {
	let primernum = document.getElementById('num1').value;
	let segundonum = document.getElementById('num2').value;
	
	let resultado = parseFloat(primernum) + parseFloat(segundonum);

	return  document.getElementById('result').value = resultado;
}

let resta = () => {
	let primernum = document.getElementById('num1').value;
	let segundonum = document.getElementById('num2').value;
	
	let resultado = parseFloat(primernum) - parseFloat(segundonum);

	return  document.getElementById('result').value = resultado;
}

let divide = () => {
	let primernum = document.getElementById('num1').value;
	let segundonum = document.getElementById('num2').value;
	
	let resultado = parseFloat(primernum) / parseFloat(segundonum);

	return  document.getElementById('result').value = resultado;
}


let multiplica = () => {
	let primernum = document.getElementById('num1').value;
	let segundonum = document.getElementById('num2').value;
	
	let resultado = parseFloat(primernum) * parseFloat(segundonum);

	return  document.getElementById('result').value = resultado;
}